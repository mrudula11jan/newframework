*** Settings ***
Documentation    This suite file verifies the valid login

Library   SeleniumLibrary
Resource    ../../resource/base/CommonFunctionalities.resource

Test Setup    Launch Browser and Navigate to URL
Test Teardown   Close Browser

*** Test Cases ***
Verify InValid Login Test
    Input Text    name=username    Admin
    Input Password   name=password    admin12333
    Click Element    xpath=//button[normalize-space()='Login']
    Element Text Should Be    xpath=//p[normalize-space()='Invalid credentials']    Invalid credentials
